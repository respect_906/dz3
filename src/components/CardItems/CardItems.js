import React, {useState} from 'react';
import {CardItem} from "../CardItem/CardItem";
import PropTypes from 'prop-types';
import "./cardItems.scss"

export const CardItems = ({items, handleModalAddOpen, handleModalRemoveOpen}) => {
    const [reload, setReload] = useState(false);
    const cardItems = items.map(item => <CardItem  {...item}
                                                   key={item.article}
                                                   handleModalAddOpen={handleModalAddOpen}
                                                   handleModalRemoveOpen={handleModalRemoveOpen}
                                                   reload={reload}
                                                   setReload={setReload}
    />);
    return (
        <div className="card-items">
            {cardItems}
        </div>
            )
};

CardItems.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object)
};

