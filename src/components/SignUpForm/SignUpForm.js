import React, {useRef} from 'react';
import axios from 'axios';

const SignUpForm = () => {
  const loginInput = useRef(null);
  const passwordInput = useRef(null);
  const handleSubmit = (e) =>{
    e.preventDefault();
    let params = {
      userName: loginInput.current.value,
      passName : passwordInput.current.value
    };
    console.log(params);
    axios.post("/users", params)
  };
  return (
    <form onSubmit={e => handleSubmit(e)} className="login">
      <input required ref={loginInput} type="text" className="login__input"
             placeholder="Enter prefered login"/>
      <input required ref={passwordInput} type="password" className="login__input"
             placeholder="Enter prefered password"/>
      <button type="submit" className="header__login-btn">Register</button>
    </form>
  );
};

export default SignUpForm;