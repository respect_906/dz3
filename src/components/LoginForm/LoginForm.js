import React, {useRef, useState} from 'react';
import {useHistory} from "react-router";

const LoginForm = ({currentUser, setCurrentUser}) => {
  const user = {
    userName:"Vet",
    passName:"123"
  };
  const history = useHistory();
  const loginInput = useRef();
  const passwordInput = useRef();
  const handleSubmit = (e) => {
    e.preventDefault();
    if ((loginInput.current.value === user.userName) && (passwordInput.current.value === user.passName)) {
      setCurrentUser({login: loginInput.current.value, password: passwordInput.current.value});
      history.push("/all");
    }
    else {
      history.push("/notAuth")
    };

  };
  const redirect = () => {
    history.push("/all")
  };
  const logOut = () => {
    setCurrentUser(null);
    history.push("/login")
  };
  if (!currentUser) {
    return (<>
      <form onSubmit={e => handleSubmit(e)} className="login">
        <input required ref={loginInput} type="text" className="login__input"
               placeholder="Enter login"/>
        <input required ref={passwordInput} type="password" className="login__input"
               placeholder="Enter password"/>
        <button type="submit" className="header__login-btn">Login</button>
      </form>
      </>
    )
  }
  else return (<div>
    You are already autorized with user <h2>{currentUser.login}</h2>
    Please click "Continue" button to continue using this app or logout to login with another user
    <button onClick={redirect}>Continue</button>
    <button onClick={logOut}>Logout</button>
  </div>)
};

export default LoginForm;