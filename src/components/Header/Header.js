import React from 'react';
import {NavLink} from 'react-router-dom';
import "./header.scss"

export const Header = ({currentUser}) => {
  return (
    <header>
      <h1>This is header with navigation!</h1>
      <nav className="navi">
        <ul className="navi__list">
          <li className="navi__list__item">
            <NavLink activeClassName="navi__list__item__link__active" className="navi__list__item__link"
                     to="/all">All</NavLink>
          </li>
          <li className="navi__list__item">
            <NavLink activeClassName="navi__list__item__link__active" className="navi__list__item__link"
                     to="/cart">Cart</NavLink>
          </li>
          <li className="navi__list__item">
            <NavLink activeClassName="navi__list__item__link__active" className="navi__list__item__link"
                     to="/favorite">Favorite</NavLink>
          </li>
        </ul>
        <NavLink activeClassName="navi__list__item__link__active" className="navi__list__item__link" to="/login">Login</NavLink>
        <NavLink activeClassName="navi__list__item__link__active" className="navi__list__item__link" to="/signup">SignUp</NavLink>
      </nav>
    </header>
  );
};
