import React, {useState} from 'react';
import {CardItem} from "../../components/CardItem/CardItem";
import "../../components/CardItems/cardItems.scss"

export const Favorite = ({items, handleModalAddOpen, handleModalRemoveOpen}) => {
    const [reload, setReload] = useState(false);
    const cardItems = items.map(item => {
        const i = item.article;
        if (JSON.parse(localStorage.getItem(`Product  ${i}  isFavorite`))) {
            return (
                <CardItem {...item} key={i}
                          setReload={setReload}
                          reload={reload}
                          handleModalAddOpen={handleModalAddOpen}
                          handleModalRemoveOpen={handleModalRemoveOpen}
                />
            )
        }
        else return null
    });
    return (
        <div className="card-items">
            {cardItems}
        </div>
    );
};
