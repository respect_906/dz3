import React from 'react';
import {CardItem} from "../../components/CardItem/CardItem";
import {useParams, withRouter} from "react-router-dom";

const CardPage = ({items, handleModalAddOpen, handleModalRemoveOpen}) => {
    const {idea} = useParams();
    const cardItem = items.map(item => {
        if (item.article === idea) {
           return (<CardItem {...item} key={item.article} handleModalAddOpen={handleModalAddOpen} handleModalRemoveOpen={handleModalRemoveOpen}/>)
        }
        else {
            return null
        }
    });
    return (
        <div className="card-items">
        {cardItem}
        </div>
    );
};
export default withRouter(CardPage)