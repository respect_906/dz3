import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import {CardItems} from "../components/CardItems/CardItems";
import {Favorite} from "../pages/Favorite/Favorite";
import {Cart} from "../pages/Cart/Cart";
import CardPage from "../pages/cardPage/CardPage";
import Page404 from "../pages/Page404/Page404";
import LoginForm from "../components/LoginForm/LoginForm";
import PageNonAuth from "../pages/PageNonAuth/PageNonAuth"
import SignUpPage from "../pages/signUpPage/SignUpPage";

const AppRoutes = ({items, handleModalAddOpen, handleModalRemoveOpen, currentUser, setCurrentUser}) => {
    return (
        <Switch>
            <Route exact path="/login" render={() =>
                <LoginForm
                currentUser={currentUser}
                setCurrentUser={setCurrentUser}
            />}
            />
            <Route exact path="/"
                   render={() => <CardItems items={items}
                                            handleModalAddOpen={handleModalAddOpen}
                                            handleModalRemoveOpen={handleModalRemoveOpen}/>}/>
            <Route exact path="/all"><Redirect to="/"/></Route>
            <Route exact path="/favorite"
                   render={() => <Favorite items={items}
                                           handleModalAddOpen={handleModalAddOpen}
                                           handleModalRemoveOpen={handleModalRemoveOpen}/>}/>
            <Route exact path="/cart"
                   render={() => <Cart items={items}
                                       handleModalRemoveOpen={handleModalRemoveOpen}/>}/>
            <Route exact path="/every/:idea">
                <CardPage items={items}
                          handleModalAddOpen={handleModalAddOpen}
                          handleModalRemoveOpen={handleModalRemoveOpen}/>
            </Route>
          <Route exact path="/notAuth" render={() =><PageNonAuth/>}/>
          <Route exact path="/signup" render={() =><SignUpPage/>}/>
            <Route path="*" component={Page404}/>

        </Switch>
    );
};

export default AppRoutes;